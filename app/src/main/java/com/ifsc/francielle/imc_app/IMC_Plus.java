package com.ifsc.francielle.imc_app;

import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class IMC_Plus extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc__plus);
    }
    public void fazCalculo (View v){
        EditText peso_et = (EditText)findViewById(R.id.peso);
        EditText altura_et = (EditText)findViewById(R.id.altura);
        TextView resultado_tv = (TextView)findViewById(R.id.resultado);



        double peso = 0;
        double altura = 0;
        double resultado = 0;

        if(peso_et.getText().toString().equals("")){
            peso = 0;
        } else if(altura_et.getText().toString().equals("")){
            altura = 0;
        } else {
            peso = Double.parseDouble(peso_et.getText().toString());
            altura =  Double.parseDouble(altura_et.getText().toString());
        }

        ImageView img = (ImageView)findViewById(R.id.imagem);

        if(peso > 0 && altura > 0){
            resultado = peso/(altura * altura);
            String rs = "" + resultado;
            resultado_tv.setText(rs.substring(0,5));
            if(resultado <= 18.5){
                img.setImageResource(R.drawable.abaixopeso);
            } else if(resultado > 18.5 && resultado <= 24.9){
                img.setImageResource(R.drawable.normal);
            } else if(resultado > 24.9 && resultado <= 29.9)  {
                img.setImageResource(R.drawable.sobrepeso);
            } else if(resultado > 29.9 && resultado <= 34.9){
                img.setImageResource(R.drawable.obesidade1);
            }else if(resultado > 34.9 && resultado <= 39.9){
                img.setImageResource(R.drawable.obesidade2);
            }else if(resultado > 40){
                img.setImageResource(R.drawable.obesidade3);
            }
        } else {
            resultado_tv.setText("Nenhum dos valores pode ser 0 (Zero)!");
            img.setImageResource(R.drawable.perfil);
        }

    }
}
